type Environment = 'development' | 'production';

// @ts-ignore
type GenericObject = { [key: string]: any };

type RawResumeIds = number | number[];

// -----------------------------------------------------------------------------

declare var ENVIRONMENT: Environment;

// -----------------------------------------------------------------------------

interface RawResumeEntry extends Array<any> {
	[0]: string;
	[1]: RawResumeIds;
}
