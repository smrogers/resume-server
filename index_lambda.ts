import { throwError400If } from '@toshokan/common';

import getResponse from './controller';

// -----------------------------------------------------------------------------

const EMPTY_BODY = '{}';

// -----------------------------------------------------------------------------

export async function handler(event: GenericObject, context: GenericObject) {
	const start = Date.now();

	//
	console.log(JSON.stringify(event, null, 2));

	//
	const {
		requestContext: { http: { path = '', method: httpMethod = 'GET' } = {} } = {},
		body: bodyRaw,
	} = event;
	const url = path.replace('/api', '');

	//
	console.log('#http #start #%s %s', httpMethod, url);

	//
	try {
		throwError400If(httpMethod.toLowerCase() === 'POST', 'Not a post method');

		//
		const body = JSON.parse(bodyRaw || EMPTY_BODY);
		const response = await getResponse(url, body);

		//
		console.log('#http #end   #%s %s #200 %dms', httpMethod, url, Date.now() - start);
		console.log(JSON.stringify(response), '\n');

		//
		return {
			statusCode: 200,
			headers: {
				'Content-Type': 'application/json',
			},
			isBase64Encoded: false,
			body: JSON.stringify({
				success: true,
				...response,
			}),
		};
	} catch (error) {
		const { statusCode = 500, userSafeMessage = 'There was an error' } = error;

		//
		console.log('#http #end   #%s %s #%d %dms', httpMethod, url, statusCode, Date.now() - start);
		console.log('#http #error', error, '\n');

		//
		return {
			statusCode: statusCode,
			headers: {
				'Content-Type': 'application/json',
				'x-amzn-ErrorType': statusCode,
			},
			isBase64Encoded: false,
			body: JSON.stringify({ message: userSafeMessage || 'There was an error' }),
		};
	}
}
