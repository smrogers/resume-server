import { flatten } from 'lodash';
import { throwError500If } from '@toshokan/common';

import { DEFAULT_SLUG } from '../../../data/default';

import { readResumeBySlug, readResumeDataByIds } from '../../../model/resume';

// -----------------------------------------------------------------------------

const EMPTY_RESUME = 'Resume is empty';

// -----------------------------------------------------------------------------

export default async function resumeReadControllerV1(
	{ slug }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	const { structure } = await readResumeBySlug(slug || DEFAULT_SLUG, connection);
	throwError500If(!Array.isArray(structure) || structure.length < 1, EMPTY_RESUME, EMPTY_RESUME);

	const dataIds: number[] = getResumeIds(structure);
	const rawData: Database.Records = await readResumeDataByIds(dataIds, connection);
	const data: GenericObject = rawData.reduce(intoDataObject, {});

	//
	return structure.reduce(intoResumeObject(data), {});
}

// -----------------------------------------------------------------------------

function intoDataObject(obj: GenericObject, { id, value }: Database.Record): GenericObject {
	obj[id] = value;

	return obj;
}

function intoResumeObject(data: GenericObject) {
	const toDataEntry = (id: number): string => data[id];

	return (obj: GenericObject, [key, ids]: RawResumeEntry): GenericObject => {
		obj[key] = Array.isArray(ids) ? ids.map(toDataEntry) : toDataEntry(ids);

		return obj;
	};
}

// -----------------------------------------------------------------------------

function getResumeIds(structure: RawResumeEntry[] = []): number[] {
	return flatten(structure.map(toSecondEntry) || []);
}

function toSecondEntry([, secondEntry]: RawResumeEntry): RawResumeIds {
	return secondEntry;
}
