import { readResumeByHash, updateResumeData } from '../../../../model/resume';

// -----------------------------------------------------------------------------

const UNACCEPTABLE_DATA_TYPE = 'Unacceptable data type';

const ACCEPTABLE_TYPES = {
	fullName: true,
	byline: true,
	contactPhoneNumber: true,
	contactEmail: true,
	personalStatement: true,
	expertise: true,
	reference: true,
	experience: true,
};

// -----------------------------------------------------------------------------

export default async function editorResumeDataUpdateControllerV1(
	{ hash, id, value }: GenericObject,
	connection?: Database.Connection,
): Promise<void> {
	const resume: Database.Record = await readResumeByHash(hash, connection);
	await updateResumeData(resume?.id, id, value);
}
