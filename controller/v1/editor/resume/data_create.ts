import { throwError400If } from '@toshokan/common';
import {
	readResumeByHash,
	createResumeData,
	updateResumeStructureByHash,
} from '../../../../model/resume';

// -----------------------------------------------------------------------------

const UNACCEPTABLE_DATA_TYPE = 'Unacceptable data type';

const ACCEPTABLE_TYPES = {
	fullName: true,
	byline: true,
	contactPhoneNumber: true,
	contactEmail: true,
	personalStatement: true,
	expertise: true,
	reference: true,
	experience: true,
};

const IS_ARRAY = {
	fullName: false,
	byline: false,
	contactPhoneNumber: false,
	contactEmail: false,
	personalStatement: false,
	expertise: true,
	reference: true,
	experience: true,
};

const ORDER = [
	'fullName',
	'byline',
	'contactPhoneNumber',
	'contactEmail',
	'personalStatement',
	'expertise',
	'reference',
	'experience',
];

// -----------------------------------------------------------------------------

export default async function editorResumeDataCreateControllerV1(
	{ hash, type, value }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	throwError400If(!ACCEPTABLE_TYPES[type], UNACCEPTABLE_DATA_TYPE);

	//
	const resume: Database.Record = await readResumeByHash(hash, connection);
	const record: Database.Records = await createResumeData(resume?.id, type, value, connection);

	//
	const structure = (resume?.structure || []).reduce(intoDataObject, {});
	const newStructure = ORDER.map((key) => {
		const isArray = IS_ARRAY[key];
		const entry = structure[key] || [key, isArray ? [] : undefined];

		//
		if (type === key) {
			if (isArray) {
				entry[1].push(record.insertId);
			} else {
				entry[1] = record.insertId;
			}
		}

		//
		return entry;
	});
	await updateResumeStructureByHash(hash, newStructure, connection);

	//
	return {
		id: record.insertId,
	};
}

// -----------------------------------------------------------------------------

function intoDataObject(obj: GenericObject, [key, ids]): GenericObject {
	obj[key] = [key, ids];

	return obj;
}
