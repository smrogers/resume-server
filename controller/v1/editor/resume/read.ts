import { DEFAULT_HASH } from '../../../../data/default';

import { readResumeByHash, readAllResumeDataByResumeId } from '../../../../model/resume';

// -----------------------------------------------------------------------------

export default async function editorResumeReadControllerV1(
	{ hash }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	const resume = await readResumeByHash(hash || DEFAULT_HASH, connection);
	const data: Database.Records = await readAllResumeDataByResumeId(resume.id, connection);

	//
	return {
		resume,
		data,
	};
}
