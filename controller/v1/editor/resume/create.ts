import { v4 as uuidv4 } from 'uuid';
import { sample } from 'lodash';
import { throwError500If } from '@toshokan/common';

import ANIMAL from '../../../../data/animal';
import NATO from '../../../../data/nato';
import COLOR from '../../../../data/color';

import { createResume, readResumeExistsBySlug } from '../../../../model/resume';

// -----------------------------------------------------------------------------

export default async function editorResumeCreateControllerV1(
	payload: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	const hash: string = uuidv4();
	const slug: string = await generateSlug(100, connection);

	//
	const record = await createResume(hash, slug, connection);
	throwError500If(!record.affectedRows, "Couldn't create resume");

	//
	return {
		hash,
	};
}

// -----------------------------------------------------------------------------

async function generateSlug(triesLeft: number, connection?: Database.Connection): Promise<string> {
	const slug = [sample(COLOR), sample(NATO), sample(ANIMAL)].join('');

	//
	if (triesLeft > 0 && (await readResumeExistsBySlug(slug, connection))) {
		return generateSlug(triesLeft - 1, connection);
	}

	//
	return slug;
}
