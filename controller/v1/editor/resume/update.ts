import { updateResumeStructureByHash } from '../../../../model/resume';

import { DEFAULT_HASH } from '../../../../data/default';

// -----------------------------------------------------------------------------

export default async function editorResumeUpdateControllerV1(
	{ hash, structure }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	return updateResumeStructureByHash(hash || DEFAULT_HASH, structure, connection);
}
