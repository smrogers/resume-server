import { throwError500If } from '@toshokan/common';

import { DEFAULT_HASH } from '../../../../data/default';

import { deleteResumeByHash } from '../../../../model/resume';

// -----------------------------------------------------------------------------

export default async function editorResumeDeleteControllerV1(
	{ hash }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	throwError500If(hash === DEFAULT_HASH, 'Tried to delete default hash');

	//
	await deleteResumeByHash(hash, connection);

	//
	return {};
}
