import { identity } from 'lodash';

import {
	deleteResumeDataByResumeIdAndId,
	readResumeByHash,
	updateResumeStructureByHash,
} from '../../../../model/resume';

// -----------------------------------------------------------------------------

export default async function editorResumeDataDeleteControllerV1(
	{ hash, id }: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	if (!hash || !id) {
		return {};
	}

	//
	const resume: Database.Record = await getResume(hash, connection);
	if (!resume) {
		return {};
	}

	//
	const outId = (sid: number) => sid !== id;
	const structure = (resume?.structure || [])
		.map(([key, ids]) => {
			return Array.isArray(ids) ? [key, ids.filter(outId)] : ids === id ? undefined : [key, ids];
		})
		.filter(identity);

	//
	await updateResumeStructureByHash(hash, structure, connection);
	await deleteResumeDataByResumeIdAndId(resume?.id, id, connection);

	//
	return {};
}

// -----------------------------------------------------------------------------

function getResume(hash: string, connection?: Database.Connection): Promise<Database.Record> {
	try {
		return readResumeByHash(hash, connection);
	} catch (error) {
		// swallow not found error
		return undefined;
	}
}
