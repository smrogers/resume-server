import { throwError404If } from '@toshokan/common';

import resumeReadControllerV1 from './v1/resume/read';

import editorResumeCreateControllerV1 from './v1/editor/resume/create';
import editorResumeReadControllerV1 from './v1/editor/resume/read';
import editorResumeUpdateControllerV1 from './v1/editor/resume/update';
import editorResumeDeleteControllerV1 from './v1/editor/resume/delete';
import editorResumeDataCreateControllerV1 from './v1/editor/resume/data_create';
import editorResumeDataUpdateControllerV1 from './v1/editor/resume/data_update';
import editorResumeDataDeleteControllerV1 from './v1/editor/resume/data_delete';

// -----------------------------------------------------------------------------

const NOT_FOUND = 'Not found';

// -----------------------------------------------------------------------------

const ROUTE_HANDLERS = {
	// client
	'/v1/resume': resumeReadControllerV1,

	// editor
	'/v1/editor/resume': editorResumeReadControllerV1,
	'/v1/editor/resume/create': editorResumeCreateControllerV1,
	'/v1/editor/resume/update': editorResumeUpdateControllerV1,
	'/v1/editor/resume/delete': editorResumeDeleteControllerV1,
	'/v1/editor/resume/data/create': editorResumeDataCreateControllerV1,
	'/v1/editor/resume/data/update': editorResumeDataUpdateControllerV1,
	'/v1/editor/resume/data/delete': editorResumeDataDeleteControllerV1,
};

// -----------------------------------------------------------------------------

export default async function getResponse(
	path: string,
	payload: GenericObject,
	connection?: Database.Connection,
): Promise<GenericObject> {
	const routeHandler = ROUTE_HANDLERS[path];
	throwError404If(!routeHandler, NOT_FOUND);

	//
	return routeHandler(payload, connection);
}
