require('dotenv').config();

// -----------------------------------------------------------------------------

import { handler } from '../index_lambda';

// -----------------------------------------------------------------------------

(async () => {
	try {
		const response = await handler(
			{
				requestContext: {
					http: {
						path: '/v1/resume',
						method: 'POST',
					},
				},
			},
			{},
		);

		console.log(response);
		process.exit(0);
	} catch (error) {
		console.log('Error:', error);
		process.exit(1);
	}
})();
