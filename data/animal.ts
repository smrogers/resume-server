export default [
	'Aardvark',
	'Alligator',
	'Alpaca',
	'Anaconda',
	'Ant',
	'Antelope',
	'Ape',
	'Aphid',
	'Armadillo',
	'Asp',
	'Ass',
	'Baboon',
	'Badger',
	'BaldEagle',
	'Barracuda',
	'Bass',
	'BassetHound',
	'Bat',
	'Bear',
	'Beaver',
	'Bedbug',
	'Bee',
	'Beetle',
	'Bird',
	'Bison',
	'BlackPanther',
	'BlackWidowSpider',
	'BlueJay',
	'BlueWhale',
	'Bobcat',
	'Buffalo',
	'Butterfly',
	'Buzzard',
	'Camel',
	'Caribou',
	'Carp',
	'Cat',
	'Caterpillar',
	'Catfish',
	'Cheetah',
	'Chicken',
	'Chimpanzee',
	'Chipmunk',
	'Cobra',
	'Cod',
	'Condor',
	'Cougar',
	'Cow',
	'Coyote',
	'Crab',
	'Crane',
	'Cricket',
	'Crocodile',
	'Crow',
	'Cuckoo',
	'Deer',
	'Dinosaur',
	'Dog',
	'Dolphin',
	'Donkey',
	'Dove',
	'Dragonfly',
	'Duck',
	'Eagle',
	'Eel',
	'Elephant',
	'Emu',
	'Falcon',
	'Ferret',
	'Finch',
	'Fish',
	'Flamingo',
	'Flea',
	'Fly',
	'Fox',
	'Frog',
	'Goat',
	'Goose',
	'Gopher',
	'Gorilla',
	'Grasshopper',
	'Hamster',
	'Hare',
	'Hawk',
	'Hippopotamus',
	'Horse',
	'Hummingbird',
	'HumpbackWhale',
	'Husky',
	'Iguana',
	'Impala',
	'Kangaroo',
	'Ladybug',
	'Leopard',
	'Lion',
	'Lizard',
	'Llama',
	'Lobster',
	'Mongoose',
	'MonitorLizard',
	'Monkey',
	'Moose',
	'Mosquito',
	'Moth',
	'MountainGoat',
	'Mouse',
	'Mule',
	'Octopus',
	'Orca',
	'Ostrich',
	'Otter',
	'Owl',
	'Ox',
	'Oyster',
	'Panda',
	'Parrot',
	'Peacock',
	'Pelican',
	'Penguin',
	'Perch',
	'Pheasant',
	'Pig',
	'Pigeon',
	'PolarBear',
	'Porcupine',
	'Quail',
	'Rabbit',
	'Raccoon',
	'Rat',
	'RattleSnake',
	'Raven',
	'Rooster',
	'SeaLion',
	'Sheep',
	'Shrew',
	'Skunk',
	'Snail',
	'Snake',
	'Spider',
	'Tiger',
	'Walrus',
	'Whale',
	'Wolf',
	'Zebra',
];
