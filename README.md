# Steven Rogers' Resume API Server

The resume API server project uses babel and ts-node to compile and run a Typescript-enabled custom-built lambda handler for rest-like calls. It replaces Express with a smaller, faster routing function. And uses my personal db library to access a mysql store (5.7.22+). Serverless is used to deploy the code to AWS.

## Running locally

Create an .env file:

```bash
ENVIRONMENT=development
DATABASE_URL=mysql://username:password@localhost:3306/schema
PORT=8889
```

Then `npm install` and `npm start`.

## Deploying to AWS

- Have local aws credentials profile called resume with generous S3, lambda, API Gateway, IAM, CloudWatch logs, and CloudFormation permissions
- Create an .env.production file:

```bash
ENVIRONMENT=production
DATABASE_URL=mysql://realusername:realpassword@realhost.com:3306/realschema
```

- `npm run deploy-production`

## Other repos

**Resume Client**: https://bitbucket.org/smrogers/resume-client

**Editor Client**: https://bitbucket.org/smrogers/resume-editor-client
