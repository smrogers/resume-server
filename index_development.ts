require('dotenv').config();

// -----------------------------------------------------------------------------

import * as http from 'http';

import timestamp from './lib/timestamp';

import getResponse from './controller';

// -----------------------------------------------------------------------------

const { PORT = 8889 } = process.env;

// -----------------------------------------------------------------------------

export const server = http.createServer(onRequest);

server.listen(PORT, () => {
	console.log('\n%s #preflight Listening on http://localhost:%d', timestamp(), PORT);
});

// -----------------------------------------------------------------------------

function onRequest(req: any, res: any): void {
	const { url } = req;
	const state = {
		method: req.method.toLowerCase(),
		now: Date.now(),
		res,
		url,
	};

	console.log('%s #http #start #%s %s', timestamp(), state.method, url);

	//
	if (state.method !== 'post') {
		return respond(state, 400);
	}

	//
	let bodyRaw = '';
	req.on('data', (chunk: any) => (bodyRaw += chunk.toString()));
	req.on('end', async () => {
		try {
			const body = parseBody(bodyRaw);
			console.log(JSON.stringify(body), '\n');

			const response = await getResponse(url, body);
			respond(state, 200, {
				success: true,
				...response,
			});
		} catch (error) {
			const { statusCode = 500, userSafeMessage = 'There was an error' } = error;

			console.log('%s #http #error #%d %s', timestamp(), statusCode, userSafeMessage);
			console.log(error, '\n');

			respond(state, statusCode, {
				success: false,
				message: userSafeMessage,
			});
		}
	});
}

// -----------------------------------------------------------------------------

function respond({ res, url, method, now }: GenericObject, code: number, payload?: any): void {
	res.setHeader('Content-Type', 'application/json');
	res.writeHead(code);
	res.end(payload !== undefined ? JSON.stringify(payload) : undefined);

	console.log('%s #http #end   #%s %s #%s %dms', timestamp(), method, url, code, Date.now() - now);
	console.log(JSON.stringify(payload), '\n');
}

function parseBody(body: string): GenericObject {
	return body.length > 0 ? JSON.parse(body) : {};
}
