import { throwError404If, throwError500If } from '@toshokan/common';

import Database from '../lib/database';

// -----------------------------------------------------------------------------

const NOT_FOUND = 'Not found';
const COUNT_DIDNT_MATCH = "Record count didn't match";

// create
// -----------------------------------------------------------------------------

export function createResume(
	hash: string,
	slug: string,
	connection?: Database.Connection,
): Promise<Database.Records> {
	return Database.insert(
		{
			into: 'resume',
			columns: ['hash', 'slug'],
			values: {
				hash,
				slug,
			},
		},
		connection,
	);
}

export function createResumeData(
	resumeId: number,
	type: string,
	value: any,
	connection?: Database.Connection,
): Promise<Database.Records> {
	return Database.insert(
		{
			into: 'resume_data',
			columns: ['resume_id', 'type', 'value'],
			values: {
				resume_id: resumeId,
				type,
				value: JSON.stringify(value),
			},
		},
		connection,
	);
}

// read
// -----------------------------------------------------------------------------

export async function readResumeExistsBySlug(
	slug: string,
	connection?: Database.Connection,
): Promise<boolean> {
	const [{ id = 0 } = {}]: Database.Records = await Database.select(
		{
			columns: {
				id: true,
			},
			from: 'resume',
			where: {
				slug,
			},
		},
		connection,
	);

	//
	return id > 0;
}

export async function readResumeBySlug(
	slug: string,
	connection?: Database.Connection,
): Promise<Database.Record> {
	debugger;
	const record: Database.Record = await Database.selectSingle(
		{
			columns: {
				id: true,
				hash: true,
				slug: true,
				structure: true,
			},
			from: 'resume',
			where: {
				slug,
			},
		},
		connection,
	);
	throwError404If(!record, NOT_FOUND);

	//
	return record;
}

export async function readResumeByHash(
	hash: string,
	connection?: Database.Connection,
): Promise<Database.Record> {
	const record: Database.Record = await Database.selectSingle(
		{
			columns: {
				id: true,
				hash: true,
				slug: true,
				structure: true,
			},
			from: 'resume',
			where: {
				hash,
			},
		},
		connection,
	);
	throwError404If(!record, NOT_FOUND);

	//
	return record;
}

export function readAllResumeDataByResumeId(
	resumeId: number,
	connection?: Database.Connection,
): Promise<Database.Records> {
	return Database.select(
		{
			columns: {
				id: true,
				type: true,
				value: true,
			},
			from: 'resume_data',
			where: {
				resume_id: resumeId,
			},
		},
		connection,
	);
}

export async function readResumeDataByIds(
	ids: number[],
	connection?: Database.Connection,
): Promise<Database.Records> {
	const records: Database.Records = ids.length
		? await Database.select(
				{
					columns: {
						id: true,
						type: true,
						value: true,
					},
					from: 'resume_data',
					where: {
						id: { $in: ids },
					},
				},
				connection,
		  )
		: ([] as Database.Records);

	//
	return records;
}

// update
// -----------------------------------------------------------------------------

export function updateResumeStructureByHash(
	hash: string,
	structure = [],
	connection?: Database.Connection,
): Promise<Database.Records> {
	return Database.update(
		{
			table: 'resume',
			set: {
				structure: JSON.stringify(structure),
			},
			where: {
				hash,
			},
		},
		connection,
	);
}

export function updateResumeData(
	resumeId: number,
	id: number,
	value: string,
	connection?: Database.Connection,
): Promise<Database.Records> {
	return Database.update(
		{
			table: 'resume_data',
			set: {
				value: JSON.stringify(value),
			},
			where: {
				resume_id: resumeId,
				id,
			},
		},
		connection,
	);
}

// delete
// -----------------------------------------------------------------------------

export function deleteResumeByHash(
	hash: string,
	connection?: Database.Connection,
): Promise<Database.Connection> {
	return Database.delete({ from: 'resume', where: { hash } }, connection);
}

export function deleteResumeDataByResumeIdAndId(
	resumeId: number,
	id: number,
	connection?: Database.Connection,
): Promise<Database.Connection> {
	return Database.delete(
		{
			from: 'resume_data',
			where: {
				id,
				resume_id: resumeId,
			},
		},
		connection,
	);
}
