import DatabaseFactory from '@toshokan/database';

import timestamp from './timestamp';

// -----------------------------------------------------------------------------

// eslint-disable-next-line no-process-env
const {
	DATABASE_URL,
	NO_QUERY,
} = process.env;

const base = {
	poolOptions: {
		connectionLimit: 30,
		timezone: 'Z',
	},
};

// -----------------------------------------------------------------------------

const Database: Database.Interface = DatabaseFactory(
	DATABASE_URL,
	NO_QUERY
		? base
		: {
				...base,
				preQuery,
				postQuery: postQuery,
				errorQuery: postQuery,
		  }
);

export default Database;

// -----------------------------------------------------------------------------

function preQuery() {
	return {
		start: Date.now(),
	};
}

function postQuery({ connection, sql, start = 0 }) {
	const duration = Date.now() - start;

	console.log(
		timestamp(),
		`#database #connection:${connection.threadId} #query ${duration}ms\n${sql}`
	);
}