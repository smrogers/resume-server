const START: number = 11;
const END:number = 23;

// -----------------------------------------------------------------------------

export default function timestamp() {
    return new Date().toISOString().substring(START, END);
}